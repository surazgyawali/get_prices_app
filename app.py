from flask import Flask,render_template,request,jsonify
import get_price

app = Flask(__name__)

@app.route('/')
def home():
    return render_template("index.html")

@app.route('/getPrices', methods = ['POST', 'GET'])
def getting_file():

    if request.method == "POST":
        reqObj = request.get_json()
        keys = list(reqObj.keys())
        needed_string_query = reqObj[keys[0]]
        info_data = get_price.get_modelPrices(needed_string_query)
        info_data["place"] = keys[0]
        print(info_data)
        return jsonify(info_data)

    return None

if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
