# Web application setup instructionss

## System requirements

- python 3.5 or later
  - Don't forget to tick the **add to PATH** option while installing python


## Setup Instructions
- Go to the project directory from commandline ( where the requirements.txt resides)
- Install all the requirements using command below:

```shell
    pip install -r requirements.txt
```

Setup is completed.

## Running  APP

Run this command in shell after setup this will run the app.

```shell
    python app.py
```

The app will run at:
    http://0.0.0.0:5000/
You can open this address in browser to use the app.
You can check the **video** for reference.

[![Price Fetching Web Application](th.jpg)](https://youtu.be/Zo8IhWhXysk "Data Scraping")

### Screenshots:

[![UI screenshot](demo_image.png)](demo_image.png)